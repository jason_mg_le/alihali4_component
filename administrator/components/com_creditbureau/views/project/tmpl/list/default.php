<?php
defined('_JEXEC') or die('Restricted access');
?>

<form method="post" name="adminForm" id="adminForm">
    <div id="filter-bar" class="btn-toolbar">
        <div class="btn-group pull-right hidden-phone">
            <label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
            <?php echo $this->pages->getLimitBox(); ?>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th width="3%">
                    <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                </th>
                <th width="2%">#</th>
                <th width="20%">
                    <?php
                    echo JText::_('COM_CREDITBUREAU_PROJECT_A_NAME');
                    ?>
                </th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_PROJECT_E_NAME'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_PROJECT_ADDRESS_A_NAME'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_PROJECT_ADDRESS_E_NAME'); ?></th>
            </tr>
        </thead>
        <?php
        $k = 0;
        for ($i = 0; $i < count($this->rows); $i++) {
            $rows = $this->rows[$i];
            ?>
            <tr class="<?php echo "row$k"; ?>">
                <td align="center">
                    <?php echo JHTML::_('grid.id', $i, $rows->project_id); ?>
                </td>
                <td align="center">
                    <a href="<?php echo JRoute::_('index.php?option=com_creditbureau&task=project.display&view=project.detail&project_id=' . $rows->project_id) ?>">
                        <?php echo $rows->project_id; ?>
                    </a>

                </td>
                <td>
                    <?php echo $rows->p_aname; ?>
                </td>
                <td><?php echo $rows->p_ename; ?></td>
                <td><?php echo $rows->a_address; ?></td>
                <td><?php echo $rows->e_address; ?></td>
            </tr>
            <?php
            $k = 1 - $k;
        }
        ?>
        <tfoot>
            <tr>
                <td colspan="14">
                    <?php
                    if ($this->pages):
                        echo $this->pages->getListFooter();
                    endif;
                    ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="task" value="project.displays"/>
</form>