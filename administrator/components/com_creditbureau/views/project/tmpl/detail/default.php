
<form id="adminForm" name="adminForm" class="form-validate" action="index.php?option=com_creditbureau&task=project.display&view=project.list" method="POST">
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_E_AREA'); ?>:</label>
        <?php
        $areas = Cbareas::getAll();
        $area_exist = Cbareasprojects::arrayAreaProject($_GET['project_id']);
        HelperCreditbureau::renderSelectWithObject('area_id', $areas, 'ar_id', 'ar_ename', 0, true, $area_exist);
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_E_NAME'); ?>:</label>
        <input type="text" name="p_ename" size="100" value="<?php echo $this->rows->p_ename; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_A_NAME'); ?>: </label>
        <input type="text" name="p_aname" size="100" value="<?php echo $this->rows->p_aname; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_ADDRESS_E_NAME'); ?>:</label>
        <input type="text" name="e_address" size="100" value="<?php echo $this->rows->e_address; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_ADDRESS_A_NAME'); ?>: </label>
        <input type="text" name="a_address" size="100" value="<?php echo $this->rows->a_address; ?>"/>
    </div>

    <input type="hidden" value="<?php echo $_GET['project_id']; ?>" name="project_id"/>
    <input type="hidden" name="option" value="com_creditbureau"/>
    <input type="hidden" name="task" value=""/> 
</form>
