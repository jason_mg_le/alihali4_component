<form id="adminForm" name="adminForm" action="index.php?option=com_creditbureau&task=project.display&view=project.list" method="POST">
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_E_AREA'); ?>:</label>
        <?php
        $areas = Cbareas::getAll();
        HelperCreditbureau::renderSelectWithObject('area_id', $areas, 'ar_id', 'ar_ename', 0,true);
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_E_NAME'); ?>:</label>
        <input type="text" name="p_ename" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_A_NAME'); ?>: </label>
        <input type="text" name="p_aname" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_ADDRESS_E_NAME'); ?>:</label>
        <input type="text" name="e_address" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_ADDRESS_A_NAME'); ?>: </label>
        <input type="text" name="a_address" size="100" value=""/>
    </div>
    <input type="hidden" name="option" value="com_creditbureau"/>
    <input type="hidden" name="task" value=""/> 
</form>