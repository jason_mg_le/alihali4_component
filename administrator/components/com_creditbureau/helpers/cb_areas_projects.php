<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.helper');

class Cbareasprojects {

    public static function add($post = array()) {
        $objTable = JTable::getInstance('CreditbureauCbareasprojects', 'Table');
        if ($objTable->bind($post)) {
            if ($objTable->store()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function update($post = array()) {
        $objTable = JTable::getInstance('CreditbureauCbareasprojects', 'Table');
        if (!$objTable->bind($post)) {
            return false;
        }
        if (!$objTable->store()) {
            return false;
        }
        return true;
    }

    public static function delete() {
        $arrId = JRequest::getVar('cid', array(), '', 'array');
        if (count($arrId) > 0) {
            $record = JTable::getInstance('CreditbureauCbareasprojects', 'Table');
            foreach ($arrId as $id) {
                if (!$record->delete($id)) {
                    JError::raiseError(500, $record->getError());
                    return false;
                }
            }
        }
        return true;
    }

    public static function getAll() {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__cb_areas_projects ORDER BY id DESC";
        $db->setQuery($query);
        $objListResult = $db->loadObjectList();
        if ($objListResult) {
            return $objListResult;
        } else {
            return array();
        }
    }

    public static function getInfoById($id) {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__cb_areas_projects WHERE id = {$id}";
        $db->setQuery($query);
        $objResult = $db->loadObject();
        if ($objResult) {
            return $objResult;
        } else {
            return array();
        }
    }

    public static function deleteByProject($project_id) {
        $db = JFactory::getDbo();
        $query = "DELETE FROM #__cb_areas_projects WHERE project_id = {$project_id}";
        $db->setQuery($query);
        $db->execute();
    }

    public static function getAreasByProjectId($project_id) {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__cb_areas_projects WHERE project_id='$project_id' ORDER BY id DESC";
        $db->setQuery($query);
        $objListResult = $db->loadObjectList();
        if ($objListResult) {
            return $objListResult;
        } else {
            return array();
        }
    }

    public static function arrayAreaProject($project_id) {
        $res = self::getAreasByProjectId($project_id);
        $list = array();
        if (count($res) > 0) {
            foreach ($res as $value) {
                $list[] = $value->area_id;
            }
        }
        return $list;
    }

}

?>