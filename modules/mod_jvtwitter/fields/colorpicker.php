<?php
/**
# mod_jvtwitter - JV Twitter
# @version		3.0
# ------------------------------------------------------------------------
# author    Open Source Code Solutions Co
# copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
# Websites: http://www.joomlavi.com
# Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');

class JFormFieldColorPicker extends JFormField {

    protected $type = 'colorpicker';

	public function getInput(){
        $document = JFactory::getDocument();
        if(version_compare(JVERSION,'3.0')<0){
            $document->addScript(JURI::root().'modules/mod_jvtwitter/assets/js/jquery.min.js');
        }
        $document->addScript(JURI::root().'modules/mod_jvtwitter/assets/js/jquery.noconflict.js');
        $document->addStyleSheet(JURI::root().'modules/mod_jvtwitter/libs/colorpicker/css/colorpicker.css');
        $document->addScript(JURI::root().'modules/mod_jvtwitter/libs/colorpicker/js/colorpicker.js');
        $document->addScript(JURI::root().'modules/mod_jvtwitter/assets/js/jvtwitter.colorpicker.js');
        $output = '<input class="inputbox jvclorpicker" id="'.$this->id.'" name="'.$this->name.'" type="text" value="'.$this->value.'" />';
        return $output;
    }
}