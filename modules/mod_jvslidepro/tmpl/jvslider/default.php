<?php
JVJSLib::$libs['jquery.plugins.jvslider'] = array(
    'require' => 'jquery.plugins.touchswipe,jquery.plugins.mousewheel,jquery.ui.effects,jquery.plugins.transform,jquery.plugins.imagesloaded,jquery.plugins.hotkey',
    'js' => JUri::root(true).'/modules/mod_jvslidepro/assets/js/jquery.jvslider.js',
    'css' => JUri::root(true).'/modules/mod_jvslidepro/assets/css/jvslider.css'
);
JVJSLib::add('jquery.plugins.jvslider');
$moduleid = "jvslider_".$module->id;
$doc->addStyleDeclaration("#{$moduleid} > ul.items > li{ width: {$dataConfigs->itemWidth} }");
$doc->addScriptDeclaration("
    jQuery(function($){
        new JVSlider('#{$moduleid}',{$dataConfigs});
    })
");
?>
<div id="<?php echo $moduleid?>" class="jvslider <?php echo $dataConfigs->theme? 'theme-'.$dataConfigs->theme:''?> <?php echo $dataConfigs->get('suffix')?>">
    <ul class="items">
        <?php  foreach($dataImages as $item): $thumb = $item->get('thumb',false); ?>
        <li class="<?php echo $item->get('type','')?>" <?php echo $thumb?'data-thumb="'.$thumb.'"':''?> data-type="<?php echo $item->get('type','')?>" data-params='<?php echo str_replace("'","\\'",$item->get('params',''))?>'>
        <?php 
            if($item->get('path')){ ?> <img src="<?php echo $item->path;?>" /><?php }
            if($item->get('title')){ ?> <div class="title"><?php echo $item->title?></div><?php } 
            if($item->get('content')){ ?> <div class="content"><?php echo $item->content?></div> <?php } 
            if($item->get('desc')){ ?> <div class="desc"><?php echo $item->desc?></div> <?php } 
            if($item->get('link')){ ?> <div class="readmore"><a href="<?php echo $item->link?>"><span><?php echo JText::_($item->readmore)?></span></a></div> <?php }
        ?>
		</li>
        <?php endforeach;
         ?>
    </ul>
    <a href="javascript:void('Prev')" class="prev"><span>Prev</span></a>
    <a href="javascript:void('Next')" class="next"><span>Next</span></a>
</div>