<?php
/**
 * @version     1.0.0
 * @package     com_portfolio
 * @copyright   Copyright (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      joomlavi <info@joomlavi.com> - http://www.joomlavi.com
 */
// no direct access
defined('_JEXEC') or die;   
$isFilter = (intval($params->get('filter')));                                                                
$isSort = intval($params->get('sort', 0));
$prefixCol = JvportfolioFrontendHelper::getPrefixCol($column);   
$ncol = intval($column)*2;
$qview = JvportfolioFrontendHelper::getActionQView("col-md-{$column}", "col-md-{$ncol}");   
?>
<?php if($items):?>
<div id="<?php echo "mod-frm-portfolio-{$module->id}"?>" class="<?php echo implode(' ', array($params->get('moduleclass_sfx', ''), $prefixCol))?>">
    <?php if($isFilter+$isSort):?>
   <div class="clearfix topPortfolio">
            <?php if($isSort):?>
                <?php require ModJvPortfolioHelper::loadTemplate('_csort')?>
            <?php endif;?>
            <?php if($isFilter):?>
                <?php require ModJvPortfolioHelper::loadTemplate('_cfilter')?>
            <?php endif;?> 

        </div>
    <?php endif;?>
    
    <div class="row">
    <div class="box-portfolio <?php echo($isFilter ? 'portfolioContainer': '')?> ">
        
        <?php foreach($items as $item):?>
		<div id="pfo-item-<?php echo $item->id?>" class="pfo-item col-md-<?php echo $column?>" data-groups='[<?php echo $item->aliasTags?>]' data-name="<?php echo strtolower($item->name)?>" data-date="<?php echo strtotime($item->date_created)?>" data-like="<?php echo $item->cliked?>">
		   <div class="p-item-img">
			  <div class="overaly">
				 
                 <div class="pfo-inner"><div class="pfo-inner2">
                 <span><a class="button-dark upp" data-qview='<?php echo preg_replace('{id}', $item->id, $qview)?>'>quick view</a></span>
				 <a class="button-dark upp"  target="_blank" href="<?php echo $item->link?>">details</a>
                  </div></div> 
                 
			  </div>
			  <img alt="<?php echo $item->name?>" src="<?php echo JUri::root().$item->image ?>">
		   </div>
		   <div class="portfolio-item-description p-item-description">

			  <?php if($params->get('showLiked', 0)):?>
			   <a class="likeheart pull-right"  href="<?php echo JUri::root()?>?option=com_jvportfolio&amp;task=items.toggleVote&amp;pfid=<?php echo $item->id?>" data-pfvote="<?php echo $item->id?>"><i class="<?php echo ($item->lactive ? 'active' : '')?> fa fa-heart pull-right">&nbsp;<?php echo $item->cliked?></i></a>
			   <?php endif;?>

			  <?php if($params->get('hasTitle', 0)):?>
			  <a class="pfo-title" href="<?php echo $item->link?>"><?php echo $item->name?></a>
			  <?php endif;?>

			  <?php if($params->get('hasTag', 0)):?>
			  <span class="gray-italic"><?php echo $item->tag?></span>
			  <?php endif;?>
			  <?php if($params->get('hasDate', 0)):?>
			  <span class="gray-italic"><?php echo date('d-m-Y', strtotime($item->date_created))?></span>
			  <?php endif;?>
		   </div>
		   <div class="bottom-border">
		   </div>
		</div>
		<?php endforeach;?>
        
        <div class="pf-load">
            <div class="box">
                <img src="<?php echo "{$com_assets}/images/load-rect.gif"?>" alt="loading"/>
                <div class=""><?php echo JText::_('Loading the next set of posts...')?></div>
            </div>
        </div>    
    </div>
    </div>
    <?php if($total):?>
    <?php require ModJvPortfolioHelper::loadTemplate('_nav')?>
    <?php endif;?>
</div>
<?php endif;?>