<form id="adminForm" name="adminForm" action="index.php?option=com_creditbureau&task=clientinfo.display&view=clientinfo.list" method="POST">
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_EN'); ?>:</label>
        <?php
        $areas = Cbprojects::getAll();
        HelperCreditbureau::renderSelectWithObject('project_id', $areas, 'project_id', 'p_ename', 0, true);
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_ANAME'); ?>: </label>
        <input type="text" name="c_aname" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_ENAME'); ?>:</label>
        <input type="text" name="c_ename" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_LOANNO'); ?>: </label>
        <input type="number" name="loans_no" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_LASTLOANSTS'); ?>: </label>
        <?php echo HelperCreditbureau::renderSelect('last_loan_st', HelperCreditbureau::statusPaidUnpaid()); ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_CARDNO'); ?>: </label>
        <input type="text" name="card_no" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_ID'); ?>: </label>
        <input type="number" name="id" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_BLACKLIST'); ?>: </label>
        <?php echo HelperCreditbureau::renderSelect('black_list', HelperCreditbureau::blackList()); ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_A_ADDRESS'); ?>: </label>
        <input type="text" name="a_address" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_E_ADDRESS'); ?>: </label>
        <input type="text" name="e_address" size="100" value=""/>
    </div><br/>
    <input class="btn btn-primary" type="submit" name="save" value="<?php echo JText::_('COM_CREDITBUREAU_SAVE'); ?>"/>
    <a class="btn btn-primary" href="<?php echo JRoute::_('index.php?option=com_creditbureau&task=clientinfo.display&view=clientinfo.list'); ?>"><?php echo JText::_('COM_CREDITBUREAU_CANCEL'); ?></a>
    <input type="hidden" name="option" value="com_creditbureau"/>
    <input type="hidden" name="task" value="clientinfo.add"/> 
</form>