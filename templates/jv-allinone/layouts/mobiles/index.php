<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>

  <?php
  echo $this['template']->render('head'); 

    include($this['path']->findPath('style.config.php'));
    ?>    
<?php 
$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
?>
</head>
<body class="<?php echo $this['option']->get('template.body.class'); ?>

<?php echo $option
  . ' view-' . $view
  . ($layout ? ' layout-' . $layout : ' no-layout')
  . ($task ? ' task-' . $task : ' no-task')
  . ($itemid ? ' itemid-' . $itemid : '')
  . ($params->get('fluidContainer') ? ' fluid' : '')
  . (isset($customdisplay) ? $customdisplay : '');
?>
">
  <div id="wrapper">
        <div id="mainsite">
            <span class="flexMenuToggle" ></span> 
            <!--Search-->
            <?php if( $this['position']->count('searchtop') ):?>
              <div id="searchtop">
                <div class="container">
                    <jdoc:include type="position" name="searchtop"  style="none"  />
                  <span id="search-beack" class="btnsearchtop"></span>
                 </div>
              </div>
            <?php endif;?>
            <!--/Search-->

            <!--Block top-->
            <?php if( $this['block']->count('panel') ):?>
              <section id="block-panel" class="top-bar">
                  <div class="container">
                    <jdoc:include type="block" name="panel" />
                    </div>
                </section>
            <?php endif;?>
            <!--/Block top-->

            <!--Block Header -->
            <header id="block-header">
                  <div class="container">
                            <jdoc:include type="position" name="logo" />
                    <?php if( $this['position']->count('menu') ):?>
                                <section id="block-mainnav">
                                        <jdoc:include type="position" name="menu" style="none" />
                            
                                </section >
                            <?php endif;?>
                            <a class="flexMenuToggle" href="JavaScript:void(0);" ><span></span><span></span><span></span></a>                
                    </div>
            </header>
            <!--/Block Header-->

            <!--Block Breadcrumb-->
            <?php if( $this['position']->count('breadcrumb') ):?>
              <section id="block-breadcrumb" class='page-title'>
                  <div class="container">
                  <jdoc:include type="position" name="breadcrumb" style="none" />
                </div>
              </section>
            <?php endif;?>
            <!--/Block Breadcrumb-->

            <!--Block bottom-->
            <?php if( $this['block']->count('fulltop') ):?>
              <section id="block-fulltop">
                      <jdoc:include type="position" name="fulltop"  />
                </section>
            <?php endif;?>
            <!--/Block bottom-->
            <section id="block-main">
                <div class="container">
                  <div class="row">
                        <?php echo $this['template']->render('content'); ?>   

                     </div>   
                </div>
            </section>
            <!--Block bottom-->
            <?php if( $this['block']->count('fullbottom') ):?>
              <section id="block-fullbottom">
                      <jdoc:include type="position" name="fullbottom"  />
                </section>
            <?php endif;?>
            <!--/Block bottom-->

            <!--Block top-->
            <?php if( $this['block']->count('top') ):?>
              <section id="block-top">
                  <div class="container">
                    <jdoc:include type="block" name="top"  />
                    </div>
                </section>
            <?php endif;?>
            <!--/Block top-->

            <!--Block bottom-->
            <?php if( $this['block']->count('user-1') ):?>
              <section id="block-user-1">
                  <div class="container">
                      <jdoc:include type="position" name="user-1"  />
                    </div>
                </section>
            <?php endif;?>
            <!--/Block bottom-->

            <!--Block bottom-->
            <?php if( $this['block']->count('user-2') ):?>
              <section id="block-user-2">
                  <div class="container">
                      <jdoc:include type="position" name="user-2"  />
                    </div>
                </section>
            <?php endif;?>
            <!--/Block user-2-->
            <!--Block user-3-->
            <?php if( $this['block']->count('user-3') ):?>
              <section id="block-user-3">
                  <div class="container">
                      <jdoc:include type="position" name="user-3"  />
                    </div>
                </section>
            <?php endif;?>
            <!--/user-3-->
            <!--user-4-->
            <?php if( $this['block']->count('user-4') ):?>
              <section id="block-user-4">
                      <jdoc:include type="position" name="user-4"  />
                </section>
            <?php endif;?>
            <!--/user-4->


            <!--user-5-->
            <?php if( $this['block']->count('user-5') ):?>
              <section id="block-user-5">
                <div class="container">
                      <jdoc:include type="position" name="user-5"  />
                          </div>
                </section>
            <?php endif;?>
            <!--/user-5->

            <!--Block topb-->
            <?php if( $this['block']->count('topb') ):?>
              <section id="block-topb">
                  <div class="container">
                    <jdoc:include type="block" name="topb" />
                    </div>
                </section>
            <?php endif;?>
            <!--/Block topb-->





            <!--user-6-->
            <?php if( $this['block']->count('user-6') ):?>
              <section id="block-user-6">
                <div class="container">
                      <jdoc:include type="position" name="user-6"  />
                          </div>
                </section>
            <?php endif;?>
            <!--/user-6->



            <!--user-7-->
            <?php if( $this['block']->count('user-7') ):?>
              <section id="block-user-7">
                <div class="container">
                      <jdoc:include type="position" name="user-7"  />
                          </div>
                </section>
            <?php endif;?>
            <!--/user-7->


            <!--user-7-->
            <?php if( $this['block']->count('user-8') ):?>
              <section id="block-user-8">
                <div class="container">
                      <jdoc:include type="position" name="user-8"  />
                          </div>
                </section>
            <?php endif;?>
            <!--/user-8->



            <!--user-9-->
            <?php if( $this['block']->count('user-9') ):?>
              <section id="block-user-9">
                <div class="container">
                      <jdoc:include type="position" name="user-9"  />
                          </div>
                </section>
            <?php endif;?>
            <!--/user-9->

            <!--user-10-->
            <?php if( $this['block']->count('user-10') ):?>
              <section id="block-user-10">
                <div class="container">
                      <jdoc:include type="position" name="user-10"  />
                          </div>
                </section>
            <?php endif;?>
            <!--/user-10->

            <!--Block Footer-->
            <?php if( $this['position']->count('footer') ):?>
              <footer id="block-footer" >
                  <div class="container">
                          <jdoc:include type="position" name="footer" style="raw"/>
                    </div>
                </footer>
            <?php endif;?>
            <!--/Block Footer-->

            <a href="#" class="back-to-top"></a>
        </div> 
  </div>
    
<?php if( $this['position']->count('color or demo') ):?>    
<script src="<?php echo $this->baseurl; ?>/templates/<?php echo JFactory::getApplication()->getTemplate(); ?>/js/layout.js" type="text/javascript"></script>
<?php endif;?>  

<?php echo $this['position']->render('analytic.none'); ?>  


</body>
</html>