<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>
<div class="row tech">

	<div id="owl-demo-11" class="owl-demo-11 owl-carousel col-xs-small cursor-move">
		<?php if(count($items)):?>

		<div class="la-rev graphic col-md-12">
		<?php foreach ($items as $key=>$item):	?>
		
		<?php if (($key%3) == 0 ):?>
			<?php if($params->get('itemImage') && isset($item->image)): ?>
			<div class="post-2">
				<img alt="<?php echo $item->title; ?>" src="<?php echo $item->image; ?>">
			</div>
			<?php endif; ?>
			<div class="post-1 mar-big">
				<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>	
					<?php if(!empty($item->event->K2CommentsCounter)): ?>
						<!-- K2 Plugins: K2CommentsCounter -->
						<?php echo $item->event->K2CommentsCounter; ?>
					<?php else: ?>
						<?php if($item->numOfComments>0): ?>
						<a class="comment c-pointer" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
							<i class="fa fa-comments"></i>&nbsp;<?php echo $item->numOfComments; ?>
						</a>
						<?php else: ?>
						<a class="comment c-pointer" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
							<i class="fa fa-comments"></i>&nbsp;0
						</a>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>
				<?php if($params->get('itemDateCreated')): ?>
				<h5>
				      <?php echo JHTML::_('date', $item->created, "d M Y"); ?>
				</h5>
				<?php endif; ?>
				<?php if($params->get('itemTitle')): ?>
				<h3 class="mar-small">
		      			<a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
				</h3>
				<?php endif; ?>
				<div class="intro">
					<?php if($params->get('itemIntroText')): ?>
					<?php echo $item->introtext; ?>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (($key%3) != 0 ):?>
			<?php if($params->get('itemImage') && isset($item->image)): ?>
			<div class="img-revievs">
				<img alt="<?php echo $item->title; ?>" src="<?php echo $item->image; ?>">
			</div>
			<?php endif; ?>

			<div class="post-1">
				<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>	
					<?php if(!empty($item->event->K2CommentsCounter)): ?>
						<!-- K2 Plugins: K2CommentsCounter -->
						<?php echo $item->event->K2CommentsCounter; ?>
					<?php else: ?>
						<?php if($item->numOfComments>0): ?>
						<a class="comment c-pointer" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
							<i class="fa fa-comments"></i>&nbsp;<?php echo $item->numOfComments; ?>
						</a>
						<?php else: ?>
						<a class="comment c-pointer" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
							<i class="fa fa-comments"></i>&nbsp;0
						</a>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>
				<?php if($params->get('itemDateCreated')): ?>
				<h5>
				      <?php echo JHTML::_('date', $item->created, "d M Y"); ?>
				</h5>
				<?php endif; ?>
				<?php if($params->get('itemTitle')): ?>
				<h3>
		      			<a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
				</h3>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if ( ((($key+1)%3) == 0) && (($key+1) != count($items)) ) {
			echo '</div>';
			echo '<div class="la-rev graphic col-md-12">';
		}?>
		<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
</div>