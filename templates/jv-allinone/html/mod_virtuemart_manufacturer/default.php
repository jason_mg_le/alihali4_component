<?php // no direct access
defined('_JEXEC') or die('Restricted access');
$classCol= ' col-sm-'.floor ( 12 / $manufacturers_per_row );
$col= 1 ;
?>
<div class="vmgroup<?php echo $params->get( 'moduleclass_sfx' ) ?>">

<?php if ($headerText) : ?>
	<div class="vmheader"><?php echo $headerText ?></div>
<?php endif;
if ($display_style =="div") { ?>
	<div class="vmmanufacturer<?php echo $params->get('moduleclass_sfx'); ?> vmManufacturer vmManufacturerDiv">
		<div class="row">
		<?php foreach ($manufacturers as $manufacturer) {
			$link = JROUTE::_('index.php?option=com_virtuemart&view=manufacturer&virtuemart_manufacturer_id=' . $manufacturer->virtuemart_manufacturer_id);

			?>
			<div class="<?php echo $classCol;?>">
				<a href="<?php echo $link; ?>">
			<?php
			if ($manufacturer->images && ($show == 'image' or $show == 'all' )) { ?>
				<?php echo $manufacturer->images[0]->displayMediaThumb('',false);?>
				<div class="bottom-border"></div>
			<?php
			}
			if ($show == 'text' or $show == 'all' ) { ?>
			 <h5><?php echo $manufacturer->mf_name; ?></h5>
			<?php
			} ?>
				</a>
			</div>
			<?php
			if ($col == $manufacturers_per_row) {
				echo "</div><div class='row'>";
				$col= 1 ;
			} else {
				$col++;
			}
		} ?>
		</div>
	</div>
<?php
} else {
?>
<div class="row">
	<div class="vmManufacturer vmManufacturerSliders vmmanufacturer<?php echo $params->get('moduleclass_sfx'); ?> listing-view-carousel" data-items="<?php echo $manufacturers_per_row;?>">
	<?php
	foreach ($manufacturers as $manufacturer) {
		$link = JROUTE::_('index.php?option=com_virtuemart&view=manufacturer&virtuemart_manufacturer_id=' . $manufacturer->virtuemart_manufacturer_id);
		?>
		<div class="vmManufacturerItem"><a href="<?php echo $link; ?>">
			<?php
			if ($manufacturer->images && ($show == 'image' or $show == 'all' )) { ?>
				<?php echo $manufacturer->images[0]->displayMediaThumb('',false);?>
				<div class="bottom-border"></div>
			<?php
			}
			if ($show == 'text' or $show == 'all' ) { ?>
			 <h5><?php echo $manufacturer->mf_name; ?></h5>
			<?php
			}
			?>
			</a>
		</div>
		<?php
	} ?>
	</div>
</div>
<?php }
	if ($footerText) : ?>
	<div class="vmfooter<?php echo $params->get( 'moduleclass_sfx' ) ?>">
		 <?php echo $footerText ?>
	</div>
<?php endif; ?>
</div>

