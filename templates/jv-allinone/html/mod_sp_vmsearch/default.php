<?php

    /**
    * VirtueMart Categories Module
    */
JHtml::_('formbehavior.chosen', '.vmSearchCategory select');
?>
<div class="<?php echo $moduleclass_sfx; ?> vmSearchCategory" id="sp-vmsearch-<?php echo $module_id ?>">
    <form action="<?php echo JRoute::_('index.php?option=com_virtuemart&view=category&search=true&limitstart=0' ); ?>" method="get">

    <div class="input-group">
      <input type="text" name="keyword" autocomplete="off" class="form-control" value="<?php echo JRequest:: getVar('keyword') ?>" />
      <select name="virtuemart_category_id" class="form-control">
            <option value="0" data-name="<?php echo JText::_('SP_VMSEARCH_ALL_CATEGORIES') ?>"><?php echo JText::_('SP_VMSEARCH_ALL_CATEGORIES') ?></option>
            <?php
                echo $modSPVMSearchHelper->getTree();
            ?>
        </select>
      <span class="input-group-btn">
        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
      </span>
    </div><!-- /input-group -->
    <div class="bottom-border"></div>

        <input type="hidden" name="limitstart" value="0" />
        <input type="hidden" name="option" value="com_virtuemart" />
        <input type="hidden" name="view" value="category" />

    </form>
</div>


<script type="text/javascript">
    jQuery(function($){
            
            // change event
            $('#sp-vmsearch-<?php echo $module_id ?> .sp-vmsearch-categories').on('change', function(event){
                    var $name = $(this).find(':selected').attr('data-name');
                    $('#sp-vmsearch-<?php echo $module_id ?> .sp-vmsearch-category-name .category-name').text($name);

            });


            // typeahed
            $('#sp-vmsearch-<?php echo $module_id ?> .sp-vmsearch-box').typeahead({
                    items  : '<?php echo $max_search_suggest; ?>',
                    source : (function(query, process){
                            return $.post('<?php echo JURI::current() ?>', 
                                { 
                                    'module_id': '<?php echo $module_id; ?>',
                                    'char': query,
                                    'category': $('#sp-vmsearch-<?php echo $module_id ?> .sp-vmsearch-categories').val()
                                }, 
                                function (data) {
                                    return process(data);
                                },'json');
                    }),
            }); 
    });
    </script>