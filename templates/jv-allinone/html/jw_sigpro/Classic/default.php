<?php
/**
 * @version		$Id: default.php 2827 2013-04-12 12:57:36Z joomlaworks $
 * @package		Simple Image Gallery Pro
 * @author		JoomlaWorks - http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		http://www.joomlaworks.net/license
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<ul id="blog-slider" class="blog-slider  <?php echo $singleThumbClass.$extraWrapperClass; ?>" data-thumb=".<?php echo "blog-slider-pager-{$gal_id}"; ?>">
	<?php foreach($gallery as $count=>$photo): ?>
	<li>

		<a href="<?php echo $photo->sourceImageFilePath; ?>" class="sigProLink<?php echo $extraClass; ?>"  title="" target="_blank"<?php echo $customLinkAttributes; ?>>
			<?php if(($gal_singlethumbmode && $count==0) || !$gal_singlethumbmode): ?>
			<img class="sigProImg" src="<?php echo $transparent; ?>" alt="<?php echo JText::_('JW_SIGP_LABELS_08').' '.$photo->filename; ?>" title="<?php echo JText::_('JW_SIGP_LABELS_08').' '.$photo->filename; ?>" style="width: 100%; height: 318px;background-image:url(<?php echo $photo->sourceImageFilePath; ?>); background-size: cover; background-position: center" />
			<?php endif; ?>
		</a>
	</li>
	<?php endforeach; ?>
</ul>


<div class="<?php echo "blog-slider-pager-{$gal_id}"?> blog-slider-pager" id="blog-slider-pager">
		<?php foreach($gallery as $count=>$photo): ?>
		 <div class="testimonials-item">
		 	<a data-slide-index="<?php echo $count; ?>" href="">
					<img class="sigProImg" src="<?php echo $transparent; ?>" alt="<?php echo JText::_('JW_SIGP_LABELS_08').' '.$photo->filename; ?>" title="<?php echo JText::_('JW_SIGP_LABELS_08').' '.$photo->filename; ?>" style="width:100%;height:100%;background-image:url(<?php echo $photo->thumbImageFilePath; ?>); background-size: cover; background-position: center" />
			</a>
		</div>
	<?php endforeach; ?>
</div>

<?php if(isset($flickrSetUrl)): ?>
<a class="sigProFlickrSetLink" title="<?php echo $flickrSetTitle; ?>" target="_blank" href="<?php echo $flickrSetUrl; ?>"><?php echo JText::_('JW_SIGP_PLG_FLICKRSET'); ?></a>
<?php endif; ?>

