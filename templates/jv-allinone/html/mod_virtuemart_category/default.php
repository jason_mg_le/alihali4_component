<?php // no direct access
defined('_JEXEC') or die('Restricted access');
//JHTML::stylesheet ( 'menucss.css', 'modules/mod_virtuemart_category/css/', false );
if(!function_exists('countProducts')){
	function countProducts($cat_id=0) {
		$db = JFactory::getDBO();
		$vendorId = 1;
		if ($cat_id > 0) {
			$q = 'SELECT count(#__virtuemart_products.virtuemart_product_id) AS total
			FROM `#__virtuemart_products`, `#__virtuemart_product_categories`
			WHERE `#__virtuemart_products`.`virtuemart_vendor_id` = "'.(int)$vendorId.'"
			AND `#__virtuemart_product_categories`.`virtuemart_category_id` = '.(int)$cat_id.'
			AND `#__virtuemart_products`.`virtuemart_product_id` = `#__virtuemart_product_categories`.`virtuemart_product_id`
			AND `#__virtuemart_products`.`published` = "1" ';
			$db->setQuery($q);
			$count = $db->loadResult();
		} else $count=0 ;

		return $count;
	}
}
/* END MODIFICATION */

if(!function_exists('getMultiCats')){
    function getMultiCats($category, $class_sfx, $active_category_id, $parentCategories){
        $showCount = ($class_sfx == "no-count")? false :true;
        $categoryModel = VmModel::getModel('Category');
        $childs = $categoryModel->getChildCategoryList(1, $category->virtuemart_category_id);
        $count = countProducts($category->virtuemart_category_id);
        $active_menu = '';
        if (in_array( $category->virtuemart_category_id, $parentCategories)) $active_menu = 'class="active opened"';
        $caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$category->virtuemart_category_id);
        $cattext = ($showCount) ?'<span class="vm-cat-name">'.$category->category_name.'</span> <span class="countProduct">('.$count.')</span>' :  $category->category_name;
        ?>
        <li <?php echo $active_menu ?>>
            <?php if($childs){?>
                <i class="fa fa-plus-square-o arrow"></i>
            <?php } else {?>
            <i class="fa fa-square-o"></i>
            <?php }?>
            <?php echo JHTML::link($caturl, $cattext); ?>
            <?php if($childs){?>
                <ul class="menu<?php echo $class_sfx; ?>">
                    <?php foreach($childs as $child) getMultiCats($child, $class_sfx, $active_category_id, $parentCategories);?>
                </ul>
            <?php }?>
        </li>
        <?php
    }
}
?>
<ul class="VMmenu vm-menu-category menu menu<?php echo $class_sfx ?> menu-sliders" >
    <?php foreach ($categories as $category) getMultiCats($category, $class_sfx, $active_category_id, $parentCategories);?>
</ul>
