<?php
/**
 * @version		$Id: user.php 1812 2013-01-14 18:45:06Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Get user stuff (do not change)
$user = JFactory::getUser();

?>

<!-- Start K2 User Layout -->

<div id="k2Container" class="userView<?php if($this->params->get('pageclass_sfx')) echo ' '.$this->params->get('pageclass_sfx'); ?>">



	<?php if ($this->params->get('userImage') || $this->params->get('userName') || $this->params->get('userDescription') || $this->params->get('userURL') || $this->params->get('userEmail')): ?>
	<div class="userBlock">

		<?php if(isset($this->addLink) && JRequest::getInt('id')==$user->id): ?>
		<!-- Item add link -->
		<span class="userItemAddLink">
			<a class="modal" rel="{handler:'iframe',size:{x:990,y:550}}" href="<?php echo $this->addLink; ?>">
				<?php echo JText::_('K2_POST_A_NEW_ITEM'); ?>
			</a>
		</span>
		<?php endif; ?>


	


<?php 
	if ( 
	$this->params->get('userImage') && !empty($this->user->avatar) ||
	$this->params->get('userName') ||
	$this->params->get('userFeedIcon',1) ||
	$this->params->get('userDescription') && trim($this->user->profile->description)
	):
?>
	<div>
    
    
    
		<?php if ($this->params->get('userImage') && !empty($this->user->avatar)): ?>
		<div class="thumb">
			<img src="<?php echo $this->user->avatar; ?>" alt="<?php echo $this->user->name; ?>"  />
			<div class="bottom-border"></div>
		</div>
		<?php endif; ?>
        <div>
        
        	<?php if($this->params->get('userFeedIcon',1)): ?>
	<!-- RSS feed icon -->
	<div class="k2FeedIcon">
		<a href="<?php echo $this->feed; ?>" title="<?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?>">
			<span class="fa fa-rss"></span>
		</a>
		
	</div>
	<?php endif; ?>
        		<?php if ($this->params->get('userName')): ?>
		<h1 class="titlePage"><?php echo $this->user->name; ?></h1>
		<?php endif; ?>
        
 		<?php if ($this->params->get('userDescription') && trim($this->user->profile->description)): ?>
		<?php echo $this->user->profile->description; ?>
		<?php endif; ?>     
        </div>
    </div>    
<?php endif; ?>       
          
		

		

		
		<?php if (($this->params->get('userURL') && isset($this->user->profile->url) && $this->user->profile->url) || $this->params->get('userEmail')): ?>
		<div class="userAdditionalInfo">
			<?php if ($this->params->get('userURL') && isset($this->user->profile->url) && $this->user->profile->url): ?>
			<div class="userURL">
				<span class="fa fa-globe"></span> <a href="<?php echo $this->user->profile->url; ?>" target="_blank" rel="me"><?php echo $this->user->profile->url; ?></a>
			</div>
			<?php endif; ?>

			<?php if ($this->params->get('userEmail')): ?>
			<div class="userEmail">
				<span class="fa fa-envelope-o"></span> <?php echo JHTML::_('Email.cloak', $this->user->email); ?>
			</div>
			<?php endif; ?>	
		</div>
		<?php endif; ?>

		
		
		<?php echo $this->user->event->K2UserDisplay; ?>
		<div class="clearfix"></div>
		
	</div>
	<?php endif; ?>



	<?php if(count($this->items)): ?>
	<!-- Item list -->
	<div class="ItemList">
		<?php foreach ($this->items as $item): ?>
		
		<!-- Start K2 Item Layout -->
		<div class="ItemView<?php if(!$item->published || ($item->publish_up != $this->nullDate && $item->publish_up > $this->now) || ($item->publish_down != $this->nullDate && $item->publish_down < $this->now)) echo ' ItemViewUnpublished'; ?><?php echo ($item->featured) ? ' ItemIsFeatured' : ''; ?> clearfix">
		
			<!-- Plugins: BeforeDisplay -->
			<?php echo $item->event->BeforeDisplay; ?>
			
			<!-- K2 Plugins: K2BeforeDisplay -->
			<?php echo $item->event->K2BeforeDisplay; ?>
		
		
		  <!-- Plugins: AfterDisplayTitle -->
		  <?php echo $item->event->AfterDisplayTitle; ?>
		  
		  <!-- K2 Plugins: K2AfterDisplayTitle -->
		  <?php echo $item->event->K2AfterDisplayTitle; ?>

		  <div class="ItemBody">

			  <?php if($this->params->get('userItemImage') && !empty($item->imageGeneric)): ?>
				<a class="ItemImage left" href="<?php echo $item->link; ?>" style="background-image: url(<?php echo $item->imageGeneric; ?>)" title="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>">
				    	<img src="<?php echo JUri::root();?>templates/<?php echo $template;?>/images/img1x1.png" alt="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>"  />
				    </a>
			  <?php endif; ?>          

			  <?php if($this->params->get('userItemTitle')): ?>
			  <!-- Item title -->
			  <h2 class="ItemTitle">
					<?php if(isset($item->editLink)): ?>
					<!-- Item edit link -->
					<span class="userItemEditLink">
						<a class="modal" rel="{handler:'iframe',size:{x:990,y:550}}" href="<?php echo $item->editLink; ?>">
							<?php echo JText::_('K2_EDIT_ITEM'); ?>
						</a>
					</span>
					<?php endif; ?>

			  	<?php if ($this->params->get('userItemTitleLinked') && $item->published): ?>
					<a href="<?php echo $item->link; ?>">
			  		<?php echo $item->title; ?>
			  	</a>
			  	<?php else: ?>
			  	<?php echo $item->title; ?>
			  	<?php endif; ?>
			  	<?php if(!$item->published || ($item->publish_up != $this->nullDate && $item->publish_up > $this->now) || ($item->publish_down != $this->nullDate && $item->publish_down < $this->now)): ?>
			  	<span>
		  			<sup>
		  				<?php echo JText::_('K2_UNPUBLISHED'); ?>
		  			</sup>
	  			</span>
	  			<?php endif; ?>
			  </h2>
			  <?php endif; ?>
              
              
			<?php if(
              $this->params->get('userItemDateCreated') ||				
              $this->params->get('userItemTags') ||
			  $this->item->params->get('itemCommentsAnchor') ||
              $this->params->get('userItemDateCreated') 
              ): ?>
              <div class="ItemLinksWork">
                    <?php if($this->params->get('userItemDateCreated')): ?>
                    <!-- Date created -->
                     <span class="ItemDateCreated">
                     <span class="fa fa-calendar"></span>
                     <span class="month">  <?php  echo JHTML::_('date',$item->created,'F');?> </span>     
                     <span class="day"> <?php echo JHTML::_('date',$item->created,'d'); ?> </span>
                     <span class="years"> <?php  echo JHTML::_('date',$item->created,'Y');?> </span>
                     </span>
                    <?php endif; ?>
                    
                    
                                    <?php if($this->params->get('ItemCategory')): ?>
                            <!-- Item category name -->
                            <span class="ItemCategory">
                                <span><?php echo JText::_('K2_PUBLISHED_IN'); ?></span>
                                <a href="<?php echo $item->category->link; ?>"><?php echo $item->category->name; ?></a>
                            </span>
                            <?php endif; ?>
                            
                    
            
                

                <?php if($this->params->get('userItemCommentsAnchor') && ( ($this->params->get('comments') == '2' && !$this->user->guest) || ($this->params->get('comments') == '1')) ): ?>
				<!-- Anchor link to comments below -->
				<span class="userItemCommentsLink">
					<?php if(!empty($item->event->K2CommentsCounter)): ?>
						<!-- K2 Plugins: K2CommentsCounter -->
						<?php echo $item->event->K2CommentsCounter; ?>
					<?php else: ?>
						<?php if($item->numOfComments > 0): ?>
						<a href="<?php echo $item->link; ?>#itemCommentsAnchor">
							<span class="fa fa-comments"></span>	<?php echo $item->numOfComments; ?> <?php echo ($item->numOfComments>1) ? JText::_('K2_COMMENTS') : JText::_('K2_COMMENT'); ?>
						</a>
						<?php else: ?>
						<a href="<?php echo $item->link; ?>#itemCommentsAnchor">
							<span class="fa fa-comments"></span>	0 <?php echo JText::_('K2_COMMENT'); ?>
						</a>
						<?php endif; ?>
					<?php endif; ?>
				</span>
				<?php endif; ?>
            
            
            
                  <?php if($this->params->get('userItemTags') && count($item->tags)): ?>
                  <!-- Item tags -->
                  <span class="ItemTags">
                      <span class="fa fa-tags"></span>
                        <?php foreach ($item->tags as $tag): ?>
                        <a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a>
                        <?php endforeach; ?>
            
                      
                  </span>
                  <?php endif; ?>
            
                    
              </div>
              <?php endif; ?>            
		
			  <!-- Plugins: BeforeDisplayContent -->
			  <?php echo $item->event->BeforeDisplayContent; ?>
			  
			  <!-- K2 Plugins: K2BeforeDisplayContent -->
			  <?php echo $item->event->K2BeforeDisplayContent; ?>
		

			  
			  <?php if($this->params->get('userItemIntroText')): ?>
			  <!-- Item introtext -->
			  <div class="ItemIntroText">
			  	<?php echo strip_tags(substr($item->introtext, 0,100)."..."); ?>
			  </div>
			  <?php endif; ?>
		
				

			  <!-- Plugins: AfterDisplayContent -->
			  <?php echo $item->event->AfterDisplayContent; ?>
			  
			  <!-- K2 Plugins: K2AfterDisplayContent -->
			  <?php echo $item->event->K2AfterDisplayContent; ?>
		
			  
		  </div>

			


			<?php if ($this->params->get('userItemReadMore')): ?>
			<!-- Item "read more..." link -->
			<div class="ItemReadMore">
				<a href="<?php echo $item->link; ?>">
					<?php echo JText::_('K2_READ_MORE'); ?>
				</a>
			</div>
			<?php endif; ?>
			
			

		  <!-- Plugins: AfterDisplay -->
		  <?php echo $item->event->AfterDisplay; ?>
		  
		  <!-- K2 Plugins: K2AfterDisplay -->
		  <?php echo $item->event->K2AfterDisplay; ?>
			
			
		</div>
		<div class="bottom-border"></div>
		<!-- End K2 Item Layout -->
		
		<?php endforeach; ?>
	</div>

	<!-- Pagination -->
	<?php if(count($this->pagination->getPagesLinks())): ?>
	<div class="jvpagination">
    		<p class="counter"><?php echo $this->pagination->getPagesCounter(); ?></p>
		<?php echo $this->pagination->getPagesLinks(); ?>
	</div>
	<?php endif; ?>
	
	<?php endif; ?>

</div>

<!-- End K2 User Layout -->
