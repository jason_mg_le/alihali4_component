<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<div class="remind <?php echo $this->pageclass_sfx?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
	<div class="page-header">
		<h1 class="titlePage">
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		</h1>
	</div>
	<?php endif; ?>

	<form id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=remind.remind'); ?>" method="post" class="form-validate form-horizontal">

		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
			<p><?php echo JText::_($fieldset->label); ?></p>

			<div class="form-horizontal">
				<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
					<div class="form-group">
						<div class="col-sm-2 control-label">
							<?php echo $field->label; ?>
						</div>
						<div class="col-sm-6">
							<?php echo $field->input; ?>
							<div class="bottom-border"></div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endforeach; ?>
		<div class="form-horizontal">
			<div class="form-group">
				<div class="col-sm-6 col-md-offset-2">
					<button type="submit" class="button-green" ><?php echo JText::_('JSUBMIT'); ?></button>
					<?php echo JHtml::_('form.token'); ?>
				</div>
			</div>
		</div>
	</form>
</div>
